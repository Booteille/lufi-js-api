# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.1] - 2020-04-13
### Fixed
* Fix an issue where the cancel message was not send to the server

## [0.4.0] - 2020-04-12
### Added
* Add a cache system
* Cache the instance list for 24 hours
* Better logging
* Now terminate the WebSocket connection when canceling an upload and the WebSocket is still trying to connect
* Messages sent in case of Failure are now a Messages enum
* onFailure() can now take a third parameter, a boolean indicating if the message came from the server

### Changed
* Refactor again the whole code to handle cancels properly

### Fixed
* Does not try to close the socket in case of spawning error.
* Error message from the server was not displayed properly
* Now retrying to connect to the socket in case of failed attempt

## [0.3.1] - 2020-04-09
### Fixed
* Fix queue broken after cancelling an upload

## [0.3.0] - 2020-04-09
### Changed
* Remove onCancel parameter from Lufi.upload() function

## [0.2.0] - 2020-04-09
### Added
* Ability to cancel uploads

### Changed
* No longer try to connect to the WebSocket after 5 fails.
* Refactor to add WebSocket queue and simplify the whole process
    * Rename LufiWS to LufiWebSocket
    * No longer need to instantiate LufiWebSocket, methods are now static.

## [0.1.5] - 2020-04-08
### Fixed
* Close connection on upload failure.

## [0.1.4] - 2020-04-08
### Added
* Add onFailure callback to the upload method.

## [0.1.3] - 2020-04-07
### Fixed
* Fix variable.json() is not a function

## [0.1.2] - 2020-04-07
### Fixed
* Fix dependencies

## [0.1.1] - 2020-04-07
Initial release.
