# Lufi JS API

**This API is experimental! Use it with caution!**

## About
An implementation allowing you to handle your Lufi files through a JS API.

## Installation
You can install it through npm:
```console
npm i lufi-js-api
```

## TODO
* Ability to select many files to upload
* Zip files before upload
* Retrieve server configuration
* ~~Cancel an upload~~
* ~~Add upload queue~~
* Better handling of UploadedFile
