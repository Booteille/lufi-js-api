"use strict";

/**
 * This contains informations about the file being upload through the WebSocket
 * @type {WebSocketFile}
 */
module.exports = class WebSocketFile {
    /**
     *
     * @param local_id
     * @param ws_url
     * @param file
     * @param slices
     * @param onReception
     * @param onSend
     * @param onCancel
     * @param onFailure
     */
    constructor(local_id, ws_url, file, slices, onReception, onSend, onCancel, onFailure) {
        this.local_id = local_id;
        this.infos = file;
        this.slices = slices;
        this.server = {
            id: null,
            index: 0,
            token: null,
            ws_url: ws_url,
        };
        this.onReception = onReception;
        this.onSend = onSend;
        this.onCancel = onCancel;
        this.onFailure = onFailure;
    }
};
