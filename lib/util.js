"use strict";

const sjcl = require("sjcl");

const generateKey = () => {
    return sjcl.codec.base64.fromBits(sjcl.random.randomWords(8, 10), 0);
};

const addTrailingSlash = (url) => {
    if (!isUrl(url)) {
        throw new TypeError("[Lufi API] - You must provide a valid URL to the function addTrailingSlash()");
    }

    return url[url.length - 1] === "/" ? url : url + "/";
};

const isUrl = (string) => {
    return new RegExp(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/).test(string);
};

const isWebSocketUrl = (string) => {
    return new RegExp(/((((ws|wss):(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/).test(string);
};

exports.addTrailingSlash = addTrailingSlash;
exports.generateKey = generateKey;
exports.isUrl = isUrl;
exports.isWebSocketUrl = isWebSocketUrl;
