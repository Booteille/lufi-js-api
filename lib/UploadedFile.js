"use strict";

module.exports = class UploadedFile {
    constructor(local_id, instance_url, server_data) {
        this.created_at = server_data.created_at;
        this.del_at_first_view = server_data.del_at_first_view;
        this.delay = server_data.delay;
        this.deleteLink = instance_url + "d/" + server_data.short + "/" + server_data.token;
        this.downloadLink = instance_url + "r/" + server_data.short + "#" + local_id;
        this.duration = server_data.duration;
        this.endpoint = instance_url;
        this.local_id = local_id;
        this.mailLink = `${instance_url}m?links=[&quot;${server_data.short}&quot;]`;
        this.name = server_data.name;
        this.slices = server_data.parts;
        this.sent_delay = server_data.sent_delay;
        this.server_id = server_data.short;
        this.server_token = server_data.token;
        this.size = server_data.size;
    }
};

