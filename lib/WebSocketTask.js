"use strict";

/**
 *
 * @type {WebSocketTask}
 */
module.exports = class WebSocketTask {
    /**
     *
     * @param id
     * @param message
     * @param index
     */
    constructor(id, message, index) {
        this.id = id;
        this.message = message;
        this.index = 0;
    }
};
