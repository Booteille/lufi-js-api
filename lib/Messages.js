"use strict";

const Messages = Object.freeze({
    CONNECTION_CANCELLED: 1,
    CONNECTION_FAIL: 2,
    CONNECTION_NOT_OPEN: 3,
    UPLOAD_FAILED: 3,
});

module.exports = Messages;
