"use strict";
const browser = require("webextension-polyfill");
const NodeCache = require("node-cache");

class LufiCache {
    static _isBrowser() {
        return typeof window !== undefined && typeof window.document !== undefined;
    }

    static _isNode() {
        return typeof process !== undefined && process.version !== null && process.versions.node !== null;
    }

    static _isWebExtension() {
        return this._isBrowser() && typeof browser !== undefined;
    }

    /**
     * Clear the cache
     * @return {Promise<void>}
     */
    static async clear() {
        if (this._isWebExtension()) {
            await browser.storage.local.clear();
        } else if (this._isBrowser()) {
            localStorage.clear();
        } else if (this._isNode()) {
            return this.nodeCache.flushAll();
        } else {
            console.error("[Lufi API][Cache] - Unable to clear the cache: Invalid environment")
        }
    }

    /**
     * Get an item from the cache
     * @param key
     * @return {Promise<null|*>}
     */
    static async get(key) {
        if (!(await this.has(key)) || await this.hasExpired(key)) {
            console.error("[Lufi API][Cache] - Unable to find this key");
        } else {
            if (await this.hasExpired(key)) {
                this.remove(key);
            } else {
                if (this._isWebExtension()) {
                    return (await browser.storage.local.get(key))[key].value;
                } else if (this._isBrowser()) {
                    return JSON.parse(localStorage.getItem(key)).value;
                } else if (this._isNode()) {
                    return NodeCache.get(key)
                } else {
                    console.error("[Lufi API][Cache] - Unable to get the value from the cache: Invalid environment")
                }
            }
        }

        return null;
    }

    /**
     * Get the date from the cache
     * @param key
     * @return {Promise<*>}
     */
    static async getDate(key) {
        if (await this.has(key)) {
            if (this._isWebExtension()) {
                return (await browser.storage.local.get(key))[key].date;
            } else if (this._isBrowser()) {
                return JSON.parse(localStorage.getItem(key)).date;
            } else {
                console.error("[Lufi API][Cache] - Unable to get the date from the cache: Invalid environment")
            }
        } else {
            console.error("[Lufi API][Cache] - Unable to find this key");
        }
    }

    /**
     * Get the Time To Live (TTL) value of an item
     * @param key
     * @return {Promise<number|*>}
     */
    static async getTTL(key) {
        if (await this.has(key)) {
            if (this._isWebExtension()) {
                return (await browser.storage.local.get(key))[key].ttl;
            } else if (this._isBrowser()) {
                return JSON.parse(localStorage.getItem(key)).ttl;
            } else if (this._isNode()) {
                return this.nodeCache.getTtl(key);
            } else {
                console.error("[Lufi API][Cache] - Unable to get the TTL from the cache: Invalid environment")
            }
        } else {
            console.error("[Lufi API][Cache] - Unable to find this key");
        }
    }

    /**
     * Check if the cache has an item
     * @param key
     * @return {Promise<boolean>}
     */
    static async has(key) {
        if (this._isWebExtension()) {
            return Object.getOwnPropertyNames((await browser.storage.local.get(key))).length > 0;
        } else if (this._isBrowser()) {
            return localStorage.getItem(key) !== null;
        } else if (this._isNode()) {
            return this.nodeCache.has(key);
        } else {
            console.error("[Lufi API][Cache] - Unable to check if the cache has this key: Invalid environment");
        }
    }

    /**
     * Check if a key has expired
     * @param key
     * @return {boolean}
     */
    static async hasExpired(key) {
        if (this._isWebExtension() || this._isBrowser()) {
            return this.getDate(key) + this.getTTL(key) * 1000 > Date.now();
        } else if (this._isNode()) {
            return await this.has(key);
        } else {
            console.error("[Lufi API][Cache] - Unable to check if the key has expired: Invalid environment");
        }
    }

    /**
     * Get an array of existing item keys
     * @return {Promise<string[]|[]|*>}
     */
    static async keys() {
        if (this._isWebExtension()) {
            return (await browser.storage.local.get()).keys();
        } else if (this._isBrowser()) {
            let keys = [];

            for (let i = 0; i < localStorage.length; i++) {
                keys.push(localStorage.key(i));
            }
            return keys;
        } else if (this._isNode()) {
            return this.nodeCache.keys();
        } else {
            console.error("[Lufi API][Cache] - Unable to to retrieve keys from the cache: Invalid environment")
        }
    }

    /**
     * Remove an item from the cache
     * @param key
     * @return {Promise<void>}
     */
    static async remove(key) {
        if (!(await this.has(key))) {
            console.error("[Lufi API][Cache] - Unable to find this key");
        } else {
            if (this._isWebExtension()) {
                await browser.storage.local.remove(key);
            } else if (this._isBrowser()) {
                localStorage.removeItem(key);
            } else if (this._isNode()) {
                this.nodeCache.del(key);
            } else {
                console.error("[Lufi API][Cache] - Unable to remove an item from the cache: Invalid environment");
            }
        }
    }

    /**
     * Add an item to the cache
     * @param key
     * @param value
     * @param ttl
     */
    static async set(key, value, ttl = 0) {
        if (this._isWebExtension()) {
            await browser.storage.local.set({[key]: {value, ttl, date: Date.now()}});
        } else if (this._isBrowser()) {
            localStorage.setItem(key, JSON.stringify({value, ttl, date: Date.now()}))
        } else if (this._isNode()) {
            this.nodeCache.set(key, value, ttl);
        } else {
            console.error("[Lufi API][Cache] - Unable to set the value to the cache: Invalid environment")
        }
    }

    /**
     * Change the TTL of an item
     * @param key
     * @param ttl
     * @return {Promise<void>}
     */
    static async setTtl(key, ttl) {
        await this.set(key, this.get(key), ttl);
    }
}

LufiCache.nodeCache = new NodeCache();

module.exports = LufiCache;
