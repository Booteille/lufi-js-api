"use strict";

const LufiCache = require("./LufiCache");
const fetch = require('node-fetch');
const CHATONS = "https://chatons.org/fr/entraide/json";

module.exports = class Instance {
    /**
     * Retrieve list of available Lufi instances
     * @param low_load
     * @returns {Promise<T | void>}
     */
    static async list(low_load = false) {
        const INSTANCE_KEY = "instances_list";

        if (await LufiCache.has(INSTANCE_KEY)) {
            console.info("[Lufi API][Instance] - Loading instance list from the cache");
            return await LufiCache.get(INSTANCE_KEY);
        } else {
            return fetch(CHATONS)
                .then(res => {
                    return res.json();
                })
                .then(async (json) => {
                    let instances = json.nodes.filter(o => o.node.software === "Lufi");

                    if (low_load) {
                        instances.filter(o => o.node.weight >= 2);
                    }

                    await LufiCache.set(INSTANCE_KEY, instances, 86400);

                    return instances;
                })
                .catch(e => console.error(e));
        }
    }

    /**
     * Select a random instance
     * @param prefer_low_load
     * @returns {Promise<*>}
     */
    static async randomInstance(prefer_low_load = true) {
        const list = await this.list(prefer_low_load);

        return list[Math.floor(Math.random() * list.length)].node;
    }

    /**
     * Get a specific known instance
     * @param name
     * @returns {Promise<string>}
     */
    static async get(name) {
        return (await this.list()).filter(o => o.node.chaton === name).node;
    }

    /**
     * Return the server WebSocket from the URL
     * @param instance_url
     */
    static getWebSocket(instance_url) {
        return instance_url.toString().replace(/http(s)?/, "ws$1") + (instance_url[instance_url.length - 1] !== "/" ? "/upload" : "upload");
    }
};
