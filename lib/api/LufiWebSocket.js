"use strict";

const {Messages} = require("../Messages");
const util = require("../util");
const WS = require("websocket").w3cwebsocket;
const Task = require("../WebSocketTask");
const WSFile = require("../WebSocketFile");

const MAX_ERRORS = 5;
let sockets = {};
let uploadingFiles = {};

module.exports = class LufiWebSocket {

    /**
     * Close the connection if the current upload must be cancelled
     * @param local_id
     * @param ws_url
     * @return {Promise<void>}
     * @private
     */
    static async _abortCurrentUpload(local_id, ws_url) {
        if (this._isSpawned(ws_url) && this._isCurrentTask(ws_url, local_id)) {
            this._close(ws_url);
        } else {
            this._cancelConnectionAttempt(ws_url);
        }
    }

    /**
     * Add a file to the uploading files list
     * @param local_id
     * @param ws_url
     * @param file
     * @param slices
     * @param onReception
     * @param onSend
     * @param onCancel
     * @param onFailure
     * @return {Promise<void>}
     * @private
     */
    static async _addFile(local_id, ws_url, file, slices, onReception, onSend, onCancel, onFailure) {
        uploadingFiles[local_id] = new WSFile(
            local_id,
            ws_url,
            file,
            slices,
            onReception,
            onSend,
            onCancel,
            onFailure
        );
    }

    /**
     * Add a task to the queue
     * @param message
     * @param id
     * @param ws_url
     * @param file
     * @param slices
     * @param onReception
     * @param onSend
     * @param onCancel
     * @param onFailure
     * @param isCancelTask
     * @return {Promise<void>}
     * @private
     */
    static async _addTask(message, id, ws_url, file, slices, onReception, onSend, onCancel, onFailure, isCancelTask = false) {
        this._spawn(ws_url, async () => {
            if (isCancelTask) { // Add cancel task to the top of the queue
                sockets[ws_url].queue.unshift(new Task(id, message, 0));
                this._runFirstTask(ws_url, false);
            } else {
                if (this._isFirstUploadTask(id)) {
                    this._addFile(id, ws_url, file, slices, onReception, onSend, onCancel, onFailure);
                }

                sockets[ws_url].queue.push(new Task(id, message, this._countTasks(ws_url, id) - 1));

                if (sockets[ws_url].queue.length === 1) {
                    this._runFirstTask(ws_url);
                }
            }
        }, onFailure);
    }

    /**
     * Remove all tasks of the socket
     * @return {Promise<void>}
     * @private
     */
    static async _cancelAllUploads() {
        for (const id in uploadingFiles) {
            this._cancelUpload(id);
        }
    }

    /**
     * Cancel the connection to a socket
     * @param ws_url
     * @return {Promise<void>}
     * @private
     */
    static async _cancelConnectionAttempt(ws_url) {
        if (this._isSocketConnecting(ws_url)) {
            this._close(ws_url);
        }
    }

    /**
     * Cancel an upload
     * @param id
     * @return {Promise<void>}
     * @private
     */
    static async _cancelUpload(id) {
        if (this._isUploadTask(id)) {
            // Ask to the server to cancel the upload
            if (uploadingFiles[id].server.id !== null) {
                this.sendCancelMessage(id, uploadingFiles[id].server.ws_url, uploadingFiles[id].server.id, uploadingFiles[id].server.token, uploadingFiles[id].server.index, uploadingFiles[id].onReception, uploadingFiles[id].onSend, uploadingFiles[id].onCancel, uploadingFiles[id].onFailure)
            }

            this._removeFileTasks(uploadingFiles[id].server.ws_url, id);

            uploadingFiles[id].onCancel();
            delete uploadingFiles[id];
        } else {
            console.error("[Lufi API][WebSocket] - This upload task does not exist")
        }
    }

    /**
     * Close a WebSocket
     * @param ws_url
     * @private
     */
    static async _close(ws_url) {
        if (this._socketExists(ws_url)) {
            console.info(`[Lufi API][WebSocket] - WebSocket ${ws_url} closed`);
            sockets[ws_url].socket.close();
        }
    }

    /**
     * Count the number of tasks bound to an id
     * @param ws_url
     * @param id
     * @return {Promise<*>}
     * @private
     */
    static async _countTasks(ws_url, id) {
        return this._getTasksOf(ws_url, id).length;
    }

    /**
     * Get all tasks from a specific id
     * @param ws_url
     * @param id
     * @return {any}
     * @private
     */
    static _getTasksOf(ws_url, id) {
        return this._socketExists(ws_url) ? sockets[ws_url].queue.filter(task => task.id === id) : [];
    }

    /**
     * Check if a WebSocket has tasks
     * @param ws_url
     * @return {boolean|boolean}
     * @private
     */
    static _hasTasks(ws_url) {
        return this._socketExists(ws_url) && sockets[ws_url].queue.length > 0;
    }

    /**
     * Check if the task is the current one
     * @param ws_url
     * @param task_id
     * @return {boolean|boolean}
     * @private
     */
    static _isCurrentTask(ws_url, task_id) {
        return this._taskExists(ws_url, task_id) && sockets[ws_url].currentTask === task_id;
    }

    /**
     * Check if the file is not already uploading
     * @param local_id
     * @return {boolean}
     * @private
     */
    static _isFirstUploadTask(local_id) {
        return uploadingFiles[local_id] === undefined;
    }

    /**
     * Check if the socket is closed
     * @param ws_url
     * @return {boolean|boolean}
     * @private
     */
    static _isSocketClose(ws_url) {
        return this._socketExists(ws_url) && sockets[ws_url].socket.readyState === WebSocket.CLOSED;
    }

    /**
     * Check if the user is connecting to the WebSocket
     * @param ws_url
     * @return {boolean|boolean}
     * @private
     */
    static _isSocketConnecting(ws_url) {
        return this._socketExists(ws_url) && sockets[ws_url].socket.readyState === WebSocket.CONNECTING;
    }

    /**
     * Check if the socket is alive
     * @param ws_url
     * @return {boolean|boolean}
     * @private
     */
    static _isSpawned(ws_url) {
        return this._socketExists(ws_url) && sockets[ws_url].socket.readyState === WebSocket.OPEN;
    }

    /**
     * Check if this task concerns an upload
     * @param id
     * @returns {boolean}
     * @private
     */
    static _isUploadTask(id) {
        return id in uploadingFiles;
    }

    /**
     * Check if an object is an instance of WebSocket
     * @param object
     * @returns {boolean}
     * @private
     */
    static _isWebSocket(object) {
        return object instanceof WebSocket;
    }

    /**
     * Receive a message from the WebSocket and run the next task
     * @param ws_url
     * @param task
     * @param runNext
     * @private
     */
    static async _receive(ws_url, task, runNext = true) {
        if (!this._isSpawned(ws_url)) {
            if (!this._isSocketConnecting(ws_url)) {
                console.error(`[Lufi API][WebSocket] - WebSocket ${ws_url} is not open`);

                if (this._isUploadTask(task.id)) {
                    uploadingFiles[task.id].onFailure(task.id, Messages.CONNECTION_NOT_OPEN);
                }
            } else {
                // Try again later
                setTimeout(async () => {
                    this._receive(ws_url, task, runNext);
                }, 250);
            }
        } else {
            sockets[ws_url].socket.onmessage = async (e) => {
                const data = JSON.parse(e.data);
                if (this._taskExists(ws_url, task)) { // Check if the task had not been removed since it started
                    if (this._isUploadTask(task.id)) {
                        console.info(`[Lufi API][WebSocket] - Listening response from the WebSocket ${ws_url}`);

                        uploadingFiles[task.id].onReception(data);
                        await this._updateFile(task.id, data);

                        if (uploadingFiles[task.id].slices === task.index + 1) {
                            delete uploadingFiles[task.id];
                        }
                    }
                } else {
                    console.info(`[Lufi API][WebSocket] - Not listening response from the WebSocket ${ws_url}. Task has been cancelled`);
                }

                if (runNext) {
                    this._runNextTask(ws_url);
                }
            }
        }
    }

    /**
     * Remove all tasks from a specific file
     * @param ws_url
     * @param local_id
     */
    static async _removeFileTasks(ws_url, local_id) {
        if (!this._hasTasks(ws_url)) {
            console.error(`[Lufi API][WebSocket] - No task to remove for the file ${local_id}`);
        } else {
            // Remove all tasks from the specific file
            sockets[ws_url].queue.filter(t => t.id === local_id).forEach((task, i) => {
                this._removeTask(ws_url, i);
            });

            console.info(`[Lufi API][WebSocket] - Cancelled tasks of the file ${local_id}`);
        }
    }

    /**
     * Remove the first task from the queue
     * @param ws_url
     * @private
     */
    static async _removeFirstTask(ws_url) {
        this._removeTask(ws_url, 0);
    }

    /**
     * Remove the task from the queue
     * @param ws_url
     * @param i
     * @private
     */
    static async _removeTask(ws_url, i) {
        if (this._taskExists(ws_url, sockets[ws_url].queue[i])) {
            sockets[ws_url].queue.splice(i, 1);
        }
    }

    /**
     * Run the first task in the queue
     * @param ws_url
     * @param runNext
     * @return {Promise<void>}
     * @private
     */
    static async _runFirstTask(ws_url, runNext = true) {
        this._runTask(ws_url, 0, runNext);
    }

    /**
     * Run the next task on the list
     * @param ws_url
     * @private
     */
    static async _runNextTask(ws_url) {
        this._removeFirstTask(ws_url);

        if (this._hasTasks(ws_url)) {
            this._runFirstTask(ws_url);
        }
    }

    /**
     * Run a specific task
     * @param ws_url
     * @param i
     * @param runNext
     * @private
     */
    static async _runTask(ws_url, i, runNext = true) {
        try {
            if (this._taskExists(ws_url, i)) {
                this._spawn(ws_url, async () => {
                    this._send(ws_url, sockets[ws_url].queue[i]);
                    this._receive(ws_url, sockets[ws_url].queue[i], runNext);

                }, sockets[ws_url].queue[i].onFailure);
            } else {
                console.error("[Lufi API][WebSocket] - This task does not exist");
            }
        } catch (e) {
            console.error(`[Lufi API][WebSocket] - ${e.message}`);

            if (this._isUploadTask(sockets[ws_url].queue[i].id)) {
                uploadingFiles[sockets[ws_url].queue[i].id].onFailure(sockets[ws_url].queue[i].id, e.message);
            }
        }
    }

    /**
     * Send data through the socket
     * @param ws_url
     * @param task
     * @private
     */
    static async _send(ws_url, task) {
        if (this._taskExists(ws_url, task)) {
            console.info(`[Lufi API][WebSocket] - Sending message to the WebSocket ${ws_url}`);

            this._spawn(ws_url, async () => {
                    sockets[ws_url].socket.send(task.message);

                    if (this._isUploadTask(task.id)) {
                        uploadingFiles[task.id].onSend();
                    }
                }, async () => {
                    if (this._isUploadTask(task.id)) {
                        uploadingFiles[task.id].onFailure(task.id, Messages.CONNECTION_FAIL);
                    }
                }
            );
        }
    }

    /**
     * Check if the socket exists
     * @param ws_url
     * @return {boolean|boolean}
     * @private
     */
    static _socketExists(ws_url) {
        return ws_url in sockets && this._isWebSocket(sockets[ws_url].socket);
    }

    /**
     * Spawn a new WebSocket connection
     * @param ws_url
     * @param callback
     * @param onFailure
     * @param error_count
     * @private
     */
    static async _spawn(ws_url, callback, onFailure, error_count = 0) {
        if (!this._isSpawned(ws_url) && !this._isSocketConnecting(ws_url)) {
            if (!util.isWebSocketUrl(ws_url)) {
                throw new Error(`The provided url is not a WebSocket url: ${ws_url}`);
            }

            if (!this._socketExists(ws_url) || this._isSocketClose(ws_url)) {
                console.info(`[Lufi API][WebSocket] - Trying to spawn WebSocket ${ws_url}`);
                sockets[ws_url] = {
                    currentTask: null,
                    queue: [],
                    socket: new WS(ws_url),
                };
            }

            sockets[ws_url].socket.onerror = async () => {
                ++error_count;
                if (error_count < 5) {
                    console.error(`[Lufi API][WebSocket] - An error happened while trying to connect to ${ws_url} - Trying again. ${error_count}/${MAX_ERRORS}`);

                    this._spawn(ws_url, callback, onFailure, error_count);
                } else {
                    const msg = `Unable to connect to ${ws_url}`;

                    onFailure(null, Messages.CONNECTION_FAIL);
                    console.error(`[Lufi API][WebSocket] - ${msg}`);
                    throw new Error(msg);
                }
            };

            sockets[ws_url].socket.onopen = async () => {
                console.info(`[Lufi API][WebSocket] - Connection Established with the WebSocket ${ws_url}`);
                callback();
            }
        } else {
            callback();
        }
    }

    /**
     * Check if the specific task exists for a WebSocket
     * @param ws_url
     * @param task
     * @return {boolean|boolean}
     * @private
     */
    static _taskExists(ws_url, task) {
        if (this._socketExists(ws_url)) {
            if (typeof task === "string") {
                return sockets[ws_url].queue.filter(t => t.id === task).length > 0;
            } else if (Number.isInteger(task)) {
                return sockets[ws_url].queue[task] !== undefined;
            } else if (typeof task === "object") {
                return sockets[ws_url].queue.includes(task)
            }
        }

        return false;
    }

    /**
     * Update uploading file with server informations
     * @param local_id
     * @param data
     * @return {Promise<void>}
     * @private
     */
    static async _updateFile(local_id, data) {
        if (data.success) {
            uploadingFiles[local_id].server.id = data.short;
            uploadingFiles[local_id].server.token = data.token;
            uploadingFiles[local_id].server.index = data.i;
        } else {
            console.error(data);
        }
    }

    /**
     * Cancel an upload
     * @param local_id
     * @return {Promise<void>}
     */
    static async cancel(local_id) {
        this._cancelUpload(local_id);
    }

    /**
     * Cancel all current uploads
     * @return {Promise<void>}
     */
    static async cancelAll() {
        this._cancelAllUploads();
    }

    /**
     * Ask to the server to cancel the upload
     * @param local_id
     * @param ws_url
     * @param server_id
     * @param server_token
     * @param queue_index
     * @param onCancel
     * @param onReception
     * @param onSend
     * @param onFailure
     */
    static async sendCancelMessage(local_id, ws_url, server_id, server_token, queue_index, onReception = () => {
    }, onSend = () => {
    }, onCancel = () => {
    }, onFailure = () => {
    }) {
        if (this._isUploadTask(local_id)) {
            const data = {
                id: server_id,
                mod_token: server_token,
                cancel: true,
                i: queue_index
            };

            await this._abortCurrentUpload(ws_url, local_id);

            console.info(`[Lufi API][WebSocket] - Asking the WebSocket ${ws_url} to cancel the upload ${server_id}`);

            // Set the new task at the top of the list.
            this._addTask(JSON.stringify(data) + "XXMOJOXXuseless", server_id, ws_url, null, null, null, null, null, null, true);
        } else {
            console.info("[Lufi API][WebSocket] - No part of the file had been send to the server yet.")
        }
    }

    /**
     * Send a message to a WebSocket
     * @param ws_url
     * @param local_id
     * @param file
     * @param message
     * @param slices
     * @param onReception
     * @param onSend
     * @param onFailure
     * @param onCancel
     */
    static async sendMessage(message, local_id, ws_url, file, slices, onReception = () => {
    }, onSend = async () => {
    }, onCancel = async () => {
    }, onFailure = async () => {
    }) {
        this._addTask(message, local_id, ws_url, file, slices, onReception, onSend, onCancel, onFailure);
    }
};
