"use strict";

const sjcl = require("sjcl");
const LufiWebSocket = require("./LufiWebSocket");
const Instance = require("./Instance");
const UploadedFile = require("../UploadedFile");
const Messages = require("../Messages");
const util = require("../util.js");

const SLICE_LENGTH = 750000; // Use slices of 0.75MB each

module.exports = class Lufi {
    /**
     * Slice a file in specific sized slices
     * @param file
     * @param slice_length
     * @returns {[]}
     */
    static _slice(file, slice_length = SLICE_LENGTH) {
        if (!(file instanceof File)) {
            console.error("[Lufi API] - You must pass a File object to the function `Lufi.slice()`");
            return [];
        }

        let slices_nb = Math.ceil(file.size / slice_length) || 1;
        let slices = [];

        let i = 0;
        for (; i < slices_nb; ++i) {
            slices.push(file.slice(i * slice_length, (i + 1) * slice_length, file.type));
        }
        return slices;
    }

    /**
     * Upload a file slice to the Lufi instance
     * @param instance_url
     * @param local_id
     * @param file
     * @param slices
     * @param current_slice
     * @param delay
     * @param del_at_first_view
     * @param zipped
     * @param server_id
     * @param server_token
     * @param queue_index
     * @param onProgress
     * @param onSuccess
     * @param onCancel
     * @param onFailure
     * @returns {Promise<void>}
     * @private
     */
    static async _uploadSlices(instance_url, local_id, file, slices, current_slice, delay, del_at_first_view, zipped, server_id, server_token, queue_index, onProgress, onSuccess, onCancel, onFailure) {
        const ws_url = Instance.getWebSocket(instance_url);

        console.info(`[Lufi API] - Sending ${file.name} to the WebSocket ${ws_url} | slice ${current_slice + 1}/${slices.length}`);

        onProgress(instance_url, local_id, file, slices, current_slice + 1);

        let fr = new FileReader();

        fr.onloadend = async () => {
            const encrypted_slice = sjcl.encrypt(local_id, window.btoa(fr.result));

            const data = {
                total: slices.length,
                part: current_slice,
                size: file.size,
                name: file.name,
                type: file.type,
                delay: delay,
                del_at_first_view: del_at_first_view,
                id: server_id,
                i: queue_index
            };

            const message = JSON.stringify(data) + "XXMOJOXX" + JSON.stringify(encrypted_slice);

            await LufiWebSocket.sendMessage(message, local_id, ws_url, file, slices, (d) => {
                if (d.success) {
                    if (slices.length === current_slice + 1) {
                        let file = new UploadedFile(local_id, instance_url, d);
                        onSuccess(file);

                        console.info(`[Lufi API] - File successfully uploaded! Download link: ${file.downloadLink}`);
                    } else {
                        this._uploadSlices(instance_url, local_id, file, slices, ++current_slice, delay, del_at_first_view, zipped, d.short, d.token, d.j, onProgress, onSuccess, onCancel, onFailure);
                    }
                } else {
                    onFailure(local_id, d.msg, true);
                }
            }, async () => {
            }, async () => {
                onCancel(local_id, ws_url, file);
            }, async () => {
                onFailure(local_id, Messages.UPLOAD_FAILED);
            });
        };
        fr.readAsBinaryString(slices[current_slice]);
    }

    /**
     * Cancel all actual uploads
     * @return {Promise<void>}
     */
    static async cancelAllUploads() {
        await LufiWebSocket.cancelAll();

        console.info("[Lufi API] - All uploads have been cancelled");
    }

    /**
     * Cancel an upload
     * @param local_id
     * @return {Promise<void>}
     */
    static async cancelUpload(local_id) {
        await LufiWebSocket.cancel(local_id);

        console.info(`[Lufi API] - The upload ${local_id} has been cancelled`);
    }

    /**
     * Retrieve informations about an upload from the instance
     * @param uploadedFile
     * @return {Promise<any>}
     */
    static async infos(uploadedFile) {
        let response = await fetch(uploadedFile.endpoint + "c", {
            method: "POST",
            headers: {
                "Accept": "application/json, text/javascript",
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
            },
            body: `short=${encodeURIComponent(uploadedFile.server_id)}&token=${encodeURIComponent(uploadedFile.server_token)}`
        });

        return await response.json();
    }

    /**
     * Ask the server to remove an uploaded file
     * @param uploadedFile
     * @return {Promise<void>}
     */
    static async remove(uploadedFile) {
        let response = await fetch(uploadedFile.deleteLink + ".json");

        if (response.json().success) {
            console.info(`[Lufi API] - The file "${name}" has been removed from the server`);
        }
    }

    /**
     * Upload a File to a specific Lufi instance
     * @param file
     * @param instance_url
     * @param delay
     * @param delete_at_first_view
     * @param onInit
     * @param onProgress
     * @param onSuccess
     * @param onCancel
     * @param onFailure
     * @returns {Promise<void>}
     */
    static async upload(file, instance_url, delay, delete_at_first_view, onInit, onProgress, onSuccess, onCancel, onFailure) {
        if (!(file instanceof File)) {
            console.error("[Lufi API] - You must pass a File object to the function `Lufi.upload()`");
            return;
        }
        const local_id = util.generateKey();

        onInit(local_id, file);

        instance_url = util.addTrailingSlash(instance_url);

        const slices = this._slice(file);

        console.info(`[Lufi API][WebSocket] - Initialize upload ${local_id}`);

        this._uploadSlices(instance_url, local_id, file, slices, 0, delay, delete_at_first_view, false, null, null, null, onProgress, onSuccess, onCancel, onFailure);
    }
};
